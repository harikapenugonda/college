/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50546
Source Host           : localhost:3306
Source Database       : college

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2018-03-30 21:09:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bus`
-- ----------------------------
DROP TABLE IF EXISTS `bus`;
CREATE TABLE `bus` (
  `bus_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_no` varchar(10) NOT NULL DEFAULT 'S1101',
  `driver_name` varchar(120) NOT NULL DEFAULT 'Raju',
  `driver_contact` text,
  `no_of_seats` int(11) NOT NULL DEFAULT '35',
  `bus_type` varchar(10) NOT NULL DEFAULT 'medium',
  `registration` varchar(45) DEFAULT NULL,
  `destination` varchar(45) NOT NULL DEFAULT 'College',
  `from_route` varchar(80) NOT NULL DEFAULT '',
  `to_route` varchar(80) NOT NULL DEFAULT '',
  `via` varchar(80) DEFAULT NULL,
  `depot` varchar(50) DEFAULT NULL,
  `entry_fee` int(11) DEFAULT NULL,
  `seat_selection` text,
  PRIMARY KEY (`bus_id`),
  UNIQUE KEY `service_no_UNIQUE` (`service_no`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='transport vehicle for the students of college';

-- ----------------------------
-- Records of bus
-- ----------------------------
INSERT INTO `bus` VALUES ('9', 'S1500', 'Rupa', '9989005448', '71', 'mini', 'TS15AK1508', 'college', 'kkd', 'jpt', 'slo', 'college', null, '1,2,49,21,15,241,221,67,69');
INSERT INTO `bus` VALUES ('11', 'S1009', 'Ramesh', 'phone:9005484424', '71', 'large', 'AP29AD1008', 'college', 'Elur', 'RJY', 'Kovvur', 'college', null, '19,67');
INSERT INTO `bus` VALUES ('12', 'S15088', 'Ramesh', 'phone: 9951548485', '71', 'large', 'AP28Ad5588', 'college', 'RJY', 'ELUR', 'KVR', 'college', null, '45,49');

-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) NOT NULL DEFAULT '',
  `duration` int(2) NOT NULL DEFAULT '3',
  `incharge` varchar(100) DEFAULT NULL,
  `sems` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `cname` (`cname`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', 'MCA', '3', null, '6');
INSERT INTO `course` VALUES ('2', 'B.TECH (CSE)', '4', null, '8');
INSERT INTO `course` VALUES ('3', 'ECE (B.TECH)', '4', null, '8');
INSERT INTO `course` VALUES ('4', 'PETROCHEM (B.TECH)', '4', null, '8');
INSERT INTO `course` VALUES ('5', 'M.Tech', '2', null, '4');
INSERT INTO `course` VALUES ('6', 'BBA', '2', '', '4');

-- ----------------------------
-- Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(100) NOT NULL DEFAULT '',
  `descripton` text,
  `date_` varchar(30) NOT NULL DEFAULT '',
  `time_` varchar(30) NOT NULL DEFAULT '',
  `notify_` int(1) NOT NULL DEFAULT '0',
  `email` int(1) NOT NULL DEFAULT '0',
  `file_` text,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of events
-- ----------------------------

-- ----------------------------
-- Table structure for `semester`
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `sem_id` int(11) NOT NULL AUTO_INCREMENT,
  `sem_num` int(11) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`sem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of semester
-- ----------------------------
INSERT INTO `semester` VALUES ('1', '1', '1', null);
INSERT INTO `semester` VALUES ('2', '2', '1', null);
INSERT INTO `semester` VALUES ('3', '3', '1', null);
INSERT INTO `semester` VALUES ('4', '4', '1', null);
INSERT INTO `semester` VALUES ('5', '5', '1', null);
INSERT INTO `semester` VALUES ('6', '6', '1', null);
INSERT INTO `semester` VALUES ('7', '1', '2', null);
INSERT INTO `semester` VALUES ('8', '2', '2', null);
INSERT INTO `semester` VALUES ('9', '3', '2', null);
INSERT INTO `semester` VALUES ('10', '4', '2', null);
INSERT INTO `semester` VALUES ('11', '5', '2', null);
INSERT INTO `semester` VALUES ('12', '6', '2', null);
INSERT INTO `semester` VALUES ('13', '7', '2', null);
INSERT INTO `semester` VALUES ('14', '8', '2', null);
INSERT INTO `semester` VALUES ('15', '1', '3', null);
INSERT INTO `semester` VALUES ('16', '2', '3', null);
INSERT INTO `semester` VALUES ('17', '3', '3', null);
INSERT INTO `semester` VALUES ('18', '4', '3', null);
INSERT INTO `semester` VALUES ('19', '5', '3', null);
INSERT INTO `semester` VALUES ('20', '6', '3', null);
INSERT INTO `semester` VALUES ('21', '7', '3', null);
INSERT INTO `semester` VALUES ('22', '8', '3', null);
INSERT INTO `semester` VALUES ('23', '1', '4', null);
INSERT INTO `semester` VALUES ('24', '2', '4', null);
INSERT INTO `semester` VALUES ('25', '3', '4', null);
INSERT INTO `semester` VALUES ('26', '4', '4', null);
INSERT INTO `semester` VALUES ('27', '5', '4', null);
INSERT INTO `semester` VALUES ('28', '6', '4', null);
INSERT INTO `semester` VALUES ('29', '7', '4', null);
INSERT INTO `semester` VALUES ('30', '8', '4', null);
INSERT INTO `semester` VALUES ('32', '1', '5', null);
INSERT INTO `semester` VALUES ('33', '2', '5', null);
INSERT INTO `semester` VALUES ('34', '3', '5', null);
INSERT INTO `semester` VALUES ('35', '4', '5', null);
INSERT INTO `semester` VALUES ('36', '1', '6', null);
INSERT INTO `semester` VALUES ('37', '2', '6', null);
INSERT INTO `semester` VALUES ('38', '3', '6', null);
INSERT INTO `semester` VALUES ('39', '4', '6', null);
INSERT INTO `semester` VALUES ('40', '1', '7', null);

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `lastname` varchar(50) DEFAULT NULL,
  `qualification` varchar(50) NOT NULL DEFAULT '',
  `course` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `bus_taken` decimal(1,0) NOT NULL DEFAULT '0',
  UNIQUE KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------

-- ----------------------------
-- Table structure for `students`
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(100) NOT NULL DEFAULT '',
  `gender` varchar(10) DEFAULT 'Male',
  `course` varchar(50) NOT NULL DEFAULT '',
  `sem` varchar(10) NOT NULL DEFAULT '',
  `bus_taken` tinyint(1) DEFAULT NULL,
  `sphoto` longtext,
  `passwd` varchar(20) NOT NULL DEFAULT '1234',
  `contact` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of students
-- ----------------------------

-- ----------------------------
-- Table structure for `subjects`
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(50) NOT NULL DEFAULT '',
  `course_id` int(2) NOT NULL DEFAULT '1',
  `sem_id` int(2) NOT NULL DEFAULT '1',
  `exam_dt` varchar(15) NOT NULL DEFAULT '',
  `incharge` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subjects
-- ----------------------------
INSERT INTO `subjects` VALUES ('1', 'ARTIFICIAL INTELLIGENCE', '1', '1', '11-03-2018', null);
INSERT INTO `subjects` VALUES ('2', 'COMPUTER SCIENCE AND ENGINEERING', '1', '1', '13-03-2018', null);
INSERT INTO `subjects` VALUES ('3', 'Discrete Mathematical Structures', '1', '1', '14-03-2018', null);
INSERT INTO `subjects` VALUES ('4', 'Management Accountancy', '1', '1', '15-03-2018', null);
INSERT INTO `subjects` VALUES ('5', 'C Language', '1', '1', '17-03-2018', null);
INSERT INTO `subjects` VALUES ('6', 'PSQT', '1', '1', '18-03-2018', null);
INSERT INTO `subjects` VALUES ('7', ' Systems Programming', '1', '2', '20-08-2018', null);
INSERT INTO `subjects` VALUES ('8', 'Principles of Programming Languages', '1', '2', '21-08-2018', null);
INSERT INTO `subjects` VALUES ('9', ' Object Oriented Programming', '1', '2', '22-08-2018', null);
INSERT INTO `subjects` VALUES ('10', 'Information Systems', '1', '2', '23-08-2018', null);
INSERT INTO `subjects` VALUES ('11', 'Organizational Behavior', '1', '2', '24-08-2018', null);
INSERT INTO `subjects` VALUES ('12', 'Data Structures Lab', '1', '2', '25-08-2018', null);
INSERT INTO `subjects` VALUES ('13', 'Theory of computation', '1', '3', '15-01-2018', null);
INSERT INTO `subjects` VALUES ('14', ' Computer Graphics', '1', '3', '16-01-2018', null);
INSERT INTO `subjects` VALUES ('15', 'File Structures', '1', '3', '17-01-2018', null);
INSERT INTO `subjects` VALUES ('16', 'Design and Analysis of algorithms', '1', '3', '18-01-2018', null);
INSERT INTO `subjects` VALUES ('17', ' Operating Systems Lab.', '1', '3', '18-01-2018', null);
INSERT INTO `subjects` VALUES ('18', ' File Structures Lab.', '1', '3', '18-01-2018', null);
INSERT INTO `subjects` VALUES ('19', 'Data Communications & Networks', '1', '4', '11-10-2018', null);
INSERT INTO `subjects` VALUES ('20', 'Data Base Management Systems', '1', '4', '12-10-2018', null);
INSERT INTO `subjects` VALUES ('21', ' Operations Research', '1', '4', '18-10-2018', null);
INSERT INTO `subjects` VALUES ('22', 'Artificial Intelligence', '1', '4', '18-10-2018', null);
INSERT INTO `subjects` VALUES ('23', 'Distributed Systems', '1', '4', '19-10-2018', null);
INSERT INTO `subjects` VALUES ('24', 'DBMS Lab', '1', '4', '20-01-2018', null);
INSERT INTO `subjects` VALUES ('25', 'ISCA', '1', '5', '11-05-2018', null);
INSERT INTO `subjects` VALUES ('26', 'Data Base Management Systems', '1', '5', '12-05-2018', null);
INSERT INTO `subjects` VALUES ('27', ' NSecurity', '1', '5', '18-05-2018', null);
INSERT INTO `subjects` VALUES ('28', 'OOSE', '1', '5', '18-05-2018', null);
INSERT INTO `subjects` VALUES ('29', 'Embedded Software System', '1', '5', '19-05-2018', null);
INSERT INTO `subjects` VALUES ('30', 'Data Warehousing', '1', '5', '20-05-2018', null);
INSERT INTO `subjects` VALUES ('31', 'Project Work', '1', '6', '15-08-2018', null);
INSERT INTO `subjects` VALUES ('38', 'OOSE', '6', '4', '12-12-2018', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `course` varchar(40) NOT NULL DEFAULT '',
  `semister` int(2) NOT NULL DEFAULT '0',
  `pending_due` int(8) NOT NULL DEFAULT '0',
  `isActive` text NOT NULL,
  `password` text NOT NULL,
  `specialization` text NOT NULL,
  `bus_acquired` tinyint(1) NOT NULL DEFAULT '0',
  `photo` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'vijay', 'MCA', '1', '0', 'true', 'vijay', 'Computers', '1', '');
INSERT INTO `users` VALUES ('2', 'rehman', 'MCA', '2', '5000', 'true', 'rehman', 'Computers', '0', '');
INSERT INTO `users` VALUES ('3', 'ramesh', 'B.Tech', '5', '1000', 'true', 'ramesh@123', 'E.C.E', '0', '');
