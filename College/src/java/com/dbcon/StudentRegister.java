package com.dbcon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.apache.catalina.util.Base64;


@WebServlet("/StudentRegister")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)

public class StudentRegister extends HttpServlet {
    private static final String SAVE_DIR="images";
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
            String savePath = "/home" + File.separator + SAVE_DIR;
                File fileSaveDir=new File(savePath);
                if(!fileSaveDir.exists()){
                    fileSaveDir.mkdir();
                }
            String sname=request.getParameter("sname");
            String course=request.getParameter("course");
            String sem = request.getParameter("sem");
            String gender = request.getParameter("gender");
            String contact = request.getParameter("contact");
            
            int bus_taken = 0;
            
            Part part=request.getPart("sphoto");
            String passwd = request.getParameter("passwd");
            String fileName=extractFileName(part);
            /*if you may have more than one files with same name then you can calculate some random characters and append that characters in fileName so that it will  make your each image name identical.*/
            System.out.println(fileName);
            part.write(savePath + File.separator +fileName);
           /* 
            //You need this loop if you submitted more than one file
            for (Part part : request.getParts()) {
            String fileName = extractFileName(part);
            part.write(savePath + File.separator + fileName);
        }*/
            try {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                }
                catch(ClassNotFoundException cnfe) {}
            Connection con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/college","root","harika555");
            String query="INSERT INTO students (sname, course, sem, bus_taken, sphoto, gender, passwd,contact) values (?, ?, ?, ?, ?, ?, ?,?)";
            
                PreparedStatement pst;
                pst=con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                pst.setString(1, sname);
                pst.setString(2,course);
                pst.setString(3, sem);
                pst.setInt(4, bus_taken);
                System.out.println(fileName);
                String filePath= savePath + File.separator + fileName ;
                pst.setString(5,getBase64String(filePath));
                System.out.println(" Hello "+ getBase64String(filePath));
                pst.setString(6, gender);
                pst.setString(7, passwd);
                pst.setString(8, contact);
                if(pst.executeUpdate()==1){
                    ResultSet keys = pst.getGeneratedKeys();    
                    keys.next();  
                    int key = keys.getInt(1);
                    HttpSession session = request.getSession();
                    session.setAttribute("st_name", sname);
                    session.setAttribute("st_id", key);
                    if(request.getParameter("transport_type").equals("bus")) response.sendRedirect("/College/student_bus.jsp?st_id="+key+"&bus=yes&msg=registered successfully");
                    else response.sendRedirect("/College/student_dashboard.jsp?st_id="+key+"&bus=no&msg=registered successfully");
                }
                else response.sendRedirect("/College/index.jsp?msg=something went wrong");
            }catch(SQLException sqe) {
                sqe.printStackTrace();
            }
    }
    // file name of the upload file is included in content-disposition header like this:
    //form-data; name="dataFile"; filename="PHOTO.JPG"
    private String extractFileName(Part part) {
        System.out.println(part.getHeader("content-disposition"));
        System.out.println("============================================");
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }
    
    private String getBase64String (String path) {
        File f = new File(path);		//change path of image according to you
		FileInputStream fis;
                String imageString= "";
        try {
            fis = new FileInputStream(f);
            System.out.println(fis);
            try {
                byte byteArray[] = new byte[(int)f.length()];
            fis.read(byteArray);
            imageString = Base64.encode(byteArray);
		fis.close();
        } catch (IOException ex) {
            Logger.getLogger(StudentRegister.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentRegister.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        
		
                return imageString;
    }
}
