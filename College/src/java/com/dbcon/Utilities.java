package com.dbcon;

import com.dbcon.Databasecon;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

public class Utilities {

    public static String save(HttpServletRequest request, String tableName) {
        return save(request, tableName, null, false, null, null);
    }

    public static String save(HttpServletRequest request, String tableName, String[] intigriyConstraints, boolean isCombinedConstrains) {
        return save(request, tableName, intigriyConstraints, isCombinedConstrains, null, null);
    }

    public static String save(HttpServletRequest request, String tableName, String[] dateParams, String dateFormat) {
        return save(request, tableName, null, false, dateParams, dateFormat);
    }

    public static boolean delete(String tableName, String columnName, String columnValue) {
        try {
            Databasecon.getconnection().createStatement().executeUpdate("delete from " + tableName + " where " + columnName + " = '" + columnValue + "'");
        } catch (SQLException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;

    }

    public static String save(HttpServletRequest request, String tableName, String[] intigriyConstraints, boolean isCombinedConstrains, String[] dateParams, String dateFormat) {
        if (request != null && tableName != null && tableName.length() > 0) {
            try {
                Connection con = Databasecon.getconnection();
                Statement stmt = con.createStatement();
                String selectQuery = "select * from " + tableName + " ";
                String criteria = "";
                String constrainType = isCombinedConstrains ? " and " : " or ";
                if (intigriyConstraints != null && intigriyConstraints.length > 0) {
                    criteria = "where ";
                    boolean isFirst = true;
                    for (String constraint : intigriyConstraints) {
                        if (isFirst) {
                            criteria += constraint + " = " + "'" + request.getParameter(constraint) + "' ";
                            isFirst = false;
                        } else {
                            criteria += constrainType + " " + constraint + " = " + "'" + request.getParameter(constraint) + "' ";
                        }
                    }
                }
                System.out.println("Criteria Query :" + selectQuery + criteria);
                ResultSet rs = stmt.executeQuery(selectQuery + criteria);

//                    DatabaseMetaData dbmd = con.getMetaData();
                    
                    ResultSetMetaData rsmd = rs.getMetaData();
                    
                    String insertQuery = "INSERT INTO " + tableName + " (###) values ($$$)";
                    String ColumnNames = "";
                    String ColumnValues = "";

                    System.out.println(rsmd.getColumnCount());

                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        if (dateParams == null || !Arrays.asList(dateParams).contains(rsmd.getColumnName(i))) {
                            if (request.getParameter(rsmd.getColumnName(i)) != null) {
                                if (ColumnNames.length() == 0) {
                                    ColumnNames += rsmd.getColumnName(i);
                                    ColumnValues += "'" + request.getParameter(rsmd.getColumnName(i)) + "'";
                                } else {
                                    ColumnNames += "," + rsmd.getColumnName(i);
                                    ColumnValues += "," + "'" + request.getParameter(rsmd.getColumnName(i)) + "'";
                                }
                            }
                        }
                    }
                    if (dateParams != null && dateFormat != null) {
                        for (String dateParam : dateParams) {
                            if (request.getParameter(dateParam) != null) {
                                if (ColumnNames.length() == 0) {
                                    ColumnNames += dateParam;
                                    ColumnValues += " STR_TO_DATE('" + request.getParameter(dateParam) + "','" + dateFormat + "')";
                                } else {
                                    ColumnNames += "," + dateParam;
                                    ColumnValues += "," + " STR_TO_DATE('" + request.getParameter(dateParam) + "','" + dateFormat + "')";
                                }
                            }
                        }
                    }

                    insertQuery = insertQuery.replace("###", ColumnNames).replace("$$$", ColumnValues);
                    System.out.println("Insert Query : " + insertQuery);
                    stmt.executeUpdate(insertQuery);
                    return "success";
                
                //con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    public static String save(Map<String, String> dataMap, String tableName) {
        return save(dataMap, tableName, null, false, null, null);
    }

    public static String save(Map<String, String> dataMap, String tableName, String[] intigriyConstraints, boolean isCombinedConstrains) {
        return save(dataMap, tableName, intigriyConstraints, isCombinedConstrains, null, null);
    }

    public static String save(Map<String, String> dataMap, String tableName, String[] dateParams, String dateFormat) {
        return save(dataMap, tableName, null, false, dateParams, dateFormat);
    }

    public static String save(Map<String, String> dataMap, String tableName, String[] intigriyConstraints, boolean isCombinedConstrains, String[] dateParams, String dateFormat) {
        if (dataMap != null && tableName != null && tableName.length() > 0) {
            try {
                Connection con = Databasecon.getconnection();
                Statement stmt = con.createStatement();
                String selectQuery = "select * from " + tableName + " ";
                String criteria = "";
                String constrainType = isCombinedConstrains ? " and " : " or ";
                if (intigriyConstraints != null && intigriyConstraints.length > 0) {
                    criteria = "where ";
                    boolean isFirst = true;
                    for (String constraint : intigriyConstraints) {
                        if (isFirst) {
                            criteria += constraint + " = " + "'" + dataMap.get(constraint) + "' ";
                            isFirst = false;
                        } else {
                            criteria += constrainType + " " + constraint + " = " + "'" + dataMap.get(constraint) + "' ";
                        }
                    }
                }
                System.out.println("Criteria Query :" + selectQuery + criteria);
                ResultSet rs = stmt.executeQuery(selectQuery + criteria);

                if (rs.next()) {
                    ResultSetMetaData rsmd = rs.getMetaData();
                    String insertQuery = "INSERT INTO " + tableName + " (###) values ($$$)";
                    String ColumnNames = "";
                    String ColumnValues = "";



                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        if (dateParams == null || !Arrays.asList(dateParams).contains(rsmd.getColumnName(i))) {
                            if (dataMap.get(rsmd.getColumnName(i)) != null) {
                                if (ColumnNames.length() == 0) {
                                    ColumnNames += rsmd.getColumnName(i);
                                    ColumnValues += "'" + dataMap.get(rsmd.getColumnName(i)) + "'";
                                } else {
                                    ColumnNames += "," + rsmd.getColumnName(i);
                                    ColumnValues += "," + "'" + dataMap.get(rsmd.getColumnName(i)) + "'";
                                }
                            }
                        }
                    }
                    if (dateParams != null && dateFormat != null) {
                        for (String dateParam : dateParams) {
                            if (dataMap.get(dateParam) != null) {
                                if (ColumnNames.length() == 0) {
                                    ColumnNames += dateParam;
                                    ColumnValues += " STR_TO_DATE('" + dataMap.get(dateParam) + "','" + dateFormat + "')";
                                } else {
                                    ColumnNames += "," + dateParam;
                                    ColumnValues += "," + " STR_TO_DATE('" + dataMap.get(dateParam) + "','" + dateFormat + "')";
                                }
                            }
                        }
                    }

                    insertQuery = insertQuery.replace("###", ColumnNames).replace("$$$", ColumnValues);
                    System.out.println("Insert Query : " + insertQuery);
                } else {
                    return "Record Already Exist";
                }
                con.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    public static int update(HttpServletRequest request, String tableName, String[] columnNames, String[] values) {
        if (request != null && tableName != null && tableName.length() > 0) {
            try {
                Connection con = Databasecon.getconnection();
                Statement stmt = con.createStatement();

                String selectQuery = "select * from " + tableName + " ";

                ResultSet rs = stmt.executeQuery(selectQuery);

                if (rs.next()) {
                    ResultSetMetaData rsmd = rs.getMetaData();
                    String updateQuery = "UPDATE " + tableName + " SET ";

                    boolean isFirst = true;

                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        if (request.getParameter(rsmd.getColumnName(i)) != null) {
                            if (isFirst) {
                                isFirst = false;
                                updateQuery += rsmd.getColumnName(i) + "='" + request.getParameter(rsmd.getColumnName(i)) + "'";
                            } else {
                                updateQuery += ", " + rsmd.getColumnName(i) + "='" + request.getParameter(rsmd.getColumnName(i)) + "'";
                            }
                        }
                    }
                    updateQuery += " where ";
                    if (columnNames == null || columnNames.length == 0) {
                        throw new Exception("unable to create query");
                    }
                    for (int i = 0; i < columnNames.length; i++) {
                        updateQuery += columnNames[i] + " = '" + values[i] + "'";
                    }
                    System.out.println(updateQuery);
                    stmt.executeUpdate(updateQuery);
                    return 100;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return -2;
            }
        } else {
            System.out.println("invalid request");
            return -1;
        }
        return 0;
    }

    public static void main(String[] args) {
//        HashMap<String, String> dataMap = new HashMap<String, String>();
//        dataMap.put("regno", "1234");
//        dataMap.put("pwd", "pwd");
//
//        save(dataMap, "students");
    }
}
