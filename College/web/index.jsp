<!DOCTYPE html>

<html>
<head>
<title>COLLEGE PROCLAMATION SYSTEM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

<style>
.dropdown 
{
    position: relative;
    display: inline-block;
}
.dropdown-content 
{
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 180px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    border-radius: 5px;
    z-index: 1;
    left:-80px;
}
.dropdown:hover .dropdown-content 
{
    display: block;
}
/* Full-width input fields */
input[type=text], input[type=password] 
{
                width: 100%;
/*                padding: 12px 20px;*/
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
}
/* Set a style for all buttons */
button 
{
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
}
button:hover 
{
                opacity: 0.8;
}
/* Extra styles for the cancel button */
.cancelbtn 
{
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
}
/* Center the image and position the close button */
.imgcontainer 
{
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
}
img.avatar 
{
                width: 40%;
                border-radius: 50%;
}
.container 
{
                padding: 16px;
}
span.psw 
{
                float: right;
                padding-top: 16px;
}
/* The Modal (background) */
.modal 
{
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 400px; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
}
/* Modal Content/Box */
.modal-content 
{
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 70%; /* Could be more or less, depending on screen size */
}
/* The Close Button (x) */
.close 
{
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
}
.close:hover,
.close:focus 
{
                color: red;
                cursor: pointer;
}
/* Add Zoom Animation */
.animate 
{
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
}
@-webkit-keyframes animatezoom 
{
            from 
            {
             -webkit-transform: scale(0)
            } 
            to 
            {
             -webkit-transform: scale(1)
            }
}
@keyframes animatezoom 
{
            from {transform: scale(0)} 
            to {transform: scale(1)}
}
/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) 
{
 span.psw 
 {
                    display: block;
                    float: none;
 }
 .cancelbtn 
 {
                    width: 100%;
 }
}
</style>
        
</head>
<body id="top">
<div class="wrapper row0">
  <div id="topbar" class="clear"> 
   <nav>
      <ul>
        <li><a href="index.jsp">Home</a></li> 
        <!--<li><a href="#">Contact Us</a></li>-->
        <div class="dropdown">
         <li class="dropbtn">Login</li>
         <div class="dropdown-content">
            <a href="student_login.jsp" style="color:purple;padding:3px 15px;font-size:15px;background:transparent;line-height:40px;">Student</a><br/>
            <a href="staff_login.jsp" style="color:purple;padding:3px 15px;font-size:15px;background:transparent;line-height:40px;">Staff</a><br/>
         </div>
        </div>
        <li><a onclick="document.getElementById('admin_login').style.display='block';">Admin Login</a></li>
      </ul>
    </nav>
  </div>
</div>
<div class="wrapper row1">
  <header id="header" class="clear"> 
   <div id="logo" class="fl_left">
      <h1>COLLEGE PROCLAMATION SYSTEM</h1>
      <p>One solution for college management</p>
   </div>
  </header>
</div>
     <% if(request.getParameter("msg")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;"><%=request.getParameter("msg")%></h3></center></div>
        <% } %>
<div class="wrapper">
  <div id="slider">
    <div id="slide-wrapper" class="rounded clear" style="max-height:450px;min-height:450px;">  
      <figure id="slide-1"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/1.jpg" alt=""></a>
      </figure>
      <figure id="slide-2"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/2.jpg" alt=""></a>
      </figure>
      <figure id="slide-3"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/3.jpg" alt=""></a>
      </figure>
      <figure id="slide-4"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/4.jpg" alt=""></a>
      </figure>
      <figure id="slide-6"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/6.jpg" alt=""></a>
      </figure>
      <figure id="slide-7"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/7.jpg" alt=""></a>
      </figure>
      <figure id="slide-8"><a class="view" href="#"><img style='height: 450px; width: 100%; object-fit: fill'src="imgs/8.jpg" alt=""></a>
      </figure>
           
    </div>
  </div>
</div>
<div id="admin_login" class="modal" > 
 <form class="modal-content animate" action="admin_home.jsp">
  <div class="imgcontainer">
   <span onclick="document.getElementById('admin_login').style.display='none'" class="close" title="Close Modal">&times;</span>
  </div>
  <center><h4 style="margin-top:35px;">Admin Login</h4></center>
  <div style="height:350px; margin:30px; padding: 30px;">
     Admin Username: <input type="text" name="stid" class="form-control" placeholder="admin" required/><br/><br/>
     Password:<input type="password" name="passwd" class="form-control" placeholder="admin" required/>
     <button type="submit">Login</button>
  </div>
 </form>
</div>
                
<script src="layout/scripts/jquery.min.js"></script> 
<script src="layout/scripts/jquery.fitvids.min.js"></script> 
<script src="layout/scripts/jquery.mobilemenu.js"></script> 
<script src="layout/scripts/tabslet/jquery.tabslet.min.js"></script>
<script type="text/javascript">
        
           $(function(){
	$('#slide-wrapper figure').fadeOut('slow').filter(':first').fadeIn('slow');
	setInterval(slideshow, 5000);
});

function slideshow() 
{
	$('#slide-wrapper figure:first').fadeOut().next().fadeIn('slow').end().appendTo('#slide-wrapper');
} 
    </script> 
</body>
</html>