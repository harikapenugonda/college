<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
      <title>Image Upload</title>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<form action="FileUploadServlet" method="post" enctype="multipart/form-data">
    <table width="400px" align="center" border=0>
       <tr>
           <td align="center" colspan=2>
            Image Details</td>
       </tr>
       <tr>
           <td>Student Name </td>
           <td>
               <input type="text" name="sname">
           </td>
       </tr>
       <tr>
           <td>Course </td>
           <td>
               <input type="text" name="course">
           </td>
       </tr>
       <tr>
           <td>Sem </td>
           <td>
               <select name="sem">
                   <option value="I">I</option>
                   <option value="II">II</option>
                   <option value="III">III</option>
                   <option value="IV">IV</option>
                   <option value="V">V</option>
                   <option value="VI">VI</option>
               </select>
           </td>
       </tr>
       <tr>
       <input type="file" name="file"/>
       </tr>
       <tr>
           <td></td>
           <td>
              <input type="submit" name="submit" value="Submit"></td>
       </tr>
   </table>
</form>
</body>
</html>