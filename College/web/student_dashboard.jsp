<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<%@page import="com.dbcon.*"%>
<%@page import="java.sql.*"%>
<%
    try {
        Connection con = Databasecon.getconnection();
        Connection con2 = Databasecon.getconnection();
        Statement stmt = con.createStatement();
        Statement stmt2 = con2.createStatement();
        ResultSet rs = null, rs2 = null;
        rs = stmt.executeQuery("select * from events");
%>
<!DOCTYPE html>

<html>
    <head>
        <title>COLLEGE PROCLAMATION SYSTEM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
            /* Full-width input fields */
            input[type=text], input[type=password] {
                width: 100%;
/*                padding: 12px 20px;*/
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            /* Set a style for all buttons */
            button {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
            }

            button:hover {
                opacity: 0.8;
            }

            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align:center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 40%;
                border-radius: 50%;
            }

            .container {
                padding: 17px;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 70%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)}
            to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)}
            to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
        </style>
        <script>
            function redirectWithBus (busId) {
                if(busId !=null) {
                    window.open("/College/student_bus.jsp?busid="+busId, "_self");
                }
            };
            function showMessage (title, time, description) {
                document.getElementById('shomsg').style.display = 'block';
                $('#ev_tit').html('title'+title);
                $('#ev_desc').html(description);
            };
            $(document).ready(function () {
                $('div.seatCharts-seat.seatCharts-cell.available').click(function () {
                    $("div.seatCharts-seat.seatCharts-cell.available").removeClass("selected");
                    window.localStorage.setItem("bus_selctd",$(this).attr("id").substring(2)+"");
                    $(this).addClass('selected');
                })
            })
        </script>

    </head>

    <body id="top">

        <% if(request.getParameter("st_id")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;">Keep note the below Username for further info: ST0000<%=request.getParameter("st_id")%></h3></center></div>
        <% } %>
        <% if(request.getParameter("msg")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;"><%=request.getParameter("msg")%></h3></center></div>
        <% } %>
<!--        <div style="display:block;"></div>-->
        <div class="wrapper row0">
            <div id="topbar" class="clear">

                <nav>
                    <ul>
                        <li style="padding:3px; background-color: khaki;border-radius: 15px;"><%="Welcome " + session.getAttribute("st_name") + ""%></li>
                        <li><a href="/College/index.jsp">Logout</a></li>

                        <!--<li><a href="admin_home.jsp">Admin Login</a></li>-->
                    </ul>
                </nav>

            </div>
        </div>



        <div class="wrapper row1">
            <header id="header" class="clear">

                <div id="logo" class="fl_left">
                    <h1><a href="index.html">COLLEGE PROCLAMATION SYSTEM</a></h1>
                    <p>One solution for college management</p>
                </div>
               </header>
        </div>



        <div class="wrapper row2">
            <div class="rounded">
                <nav id="mainav" class="clear">

                    <ul class="clear">
                        <li><h3 class="active" style="    margin: 0 0 0px 0 !important;"><a href="/College/student_dashboard.jsp">Notifications</a></h3></li>
                        <li><h3 style="    margin: 0 0 0px 0 !important;"><a href="/College/student_bus.jsp">Travel Management (Bus)</a></h3></li>
                        <li><h3 style="    margin: 0 0 0px 0 !important;"><a href="/College/viewstudent.jsp?id=<%=session.getAttribute("st_id")%>">Hall Ticket</a></h3></li>
                    </ul>

                </nav>
            </div>
        </div>

        <div class="wrapper">
            <div id="slider">
                <div id="slide-wrapper" class="rounded clear">
                    <div><table>
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Brief Content</th>
                                    <th>Time of event</th>
                                </tr>
                            </thead>
                            <hr>
                        <% while(rs.next()) {
                      %>
                      <tr>
                                    <td><%=rs.getString("event_title")%></td>
                                    <td style="    white-space: pre-wrap;
                                        ">
                                        <%=rs.getString("descripton")%></td>

                                    <td><%=rs.getString("date_")%> <%=rs.getString("time_")%></td>
                                </tr>
                    <%
                    }%>
                    </table></div>

                </div>
            </div></div>
                    <div id="shomsg" class="modal">
                    <div class="modal-content animate">
                        <h3 id="ev_tit"></h3>
                        <p id="ev_desc"></p>
                    </div>
                </div>
                <script src="layout/scripts/jquery.min.js"></script>
                <script src="layout/scripts/jquery.fitvids.min.js"></script>
                <script src="layout/scripts/jquery.mobilemenu.js"></script>
                <script src="layout/scripts/tabslet/jquery.tabslet.min.js"></script>
                <script src="layout/scripts/jquery.min.js"></script>
                <%
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                %>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>