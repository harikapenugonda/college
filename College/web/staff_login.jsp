<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<%@page import="com.dbcon.*"%>
<%@page import="java.sql.*"%>
<%
try{
    Connection con = Databasecon.getconnection();
    Connection con2 = Databasecon.getconnection();
    Statement stmt = con.createStatement();
    Statement stmt2 = con2.createStatement();
    ResultSet rs = null, rs2 = null;
       %>
<!DOCTYPE html>
<!--
Template Name: Academic Education V2
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
<title>COLLEGE PROCLAMATION SYSTEM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
            /* Full-width input fields */
            input[type=text], input[type=password] {
                width: 100%;
/*                padding: 12px 20px;*/
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            /* Set a style for all buttons */
            button {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
            }

            button:hover {
                opacity: 0.8;
            }

            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 40%;
                border-radius: 50%;
            }

            .container {
                padding: 16px;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 70%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
            to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
            to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
        </style>
   <script>
       <%
        if (request.getParameter("open_login") != null) {
    %>
        setTimeout(function() {
            document.getElementById('staff_login').style.display='block';
        },600)
    <%
        }
    %>
    <%
        rs2 = stmt2.executeQuery("select * from semester");
    %>
        $(document).ready(function() {
            var optarray = $("#semdropdown").children('option').map(function() {
        return {
            "value": this.value,
            "option": "<option value='" + this.value + "'>" + this.text + "</option>"
        }
    })
    $("#courseddown").change(function() {
        $("#semdropdown").children('option').remove();
        var addoptarr = [];
        <% rs2.beforeFirst();
        while(rs2.next()) {
            %>
                    if(<%=rs2.getInt("course_id")%>==$("#courseddown").val())
                  addoptarr.push("<option value='<%=rs2.getInt("sem_num")%>'><%=rs2.getInt("sem_num")%></option>");  
                    <%
            }
            %>
//        for (i = 0; i < optarray.length; i++) {
//            if (optarray[i].value.indexOf($(this).val()) > -1) {
//                
//            }
//        }
        $("#semdropdown").html(addoptarr.join(''))
    }).change();
        });
        
        $('#scpwd').blur (function()  {
        if($('#spwd').val()!=''||$('#spwd').val()!=null)
        if($('#spwd').val()!=$('#scpwd').val()) {
            $('#pwdError').css({'display':'block'});
        } else $('#pwdError').css({'display':'none'});
        });
    
</script>     
</head>
<body id="top">
 
 <% if(request.getParameter("msg")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;"><%=request.getParameter("msg")%></h3></center></div>
        <% } %>

<div class="wrapper row0">
  <div id="topbar" class="clear"> 
    
    <nav>
      <ul>
        <li><a href="index.jsp">Home</a></li>
        <li><a href="student_login.jsp">Student Register</a></li>
        <li><a href="staff_login.jsp">Staff Register</a></li>
      </ul>
    </nav>
     
  </div>
</div>
 
 

<div class="wrapper row1">
  <header id="header" class="clear"> 
    
    <div id="logo" class="fl_left">
      <h1><a href="index.html">COLLEGE PROCLAMATION SYSTEM</a></h1>
      <p>One solution for college management</p>
    </div>
    <!--<div class="fl_right">
      <form class="clear" method="post" action="#">
        <fieldset>
          <legend>Search:</legend>
          <input type="text" value="" placeholder="Search Here">
          <button class="fa fa-search" type="submit" title="Search"><em>Search</em></button>
        </fieldset>
      </form>
    </div>-->
     
  </header>
</div>
 
 

<div class="wrapper row2">
  <div class="rounded">
    <nav id="mainav" class="clear"> 
      
      <ul class="clear">
          <li><h3 style="    margin: 0 0 0px 0 !important;">Staff Register</h3></li>
      </ul>
       
    </nav>
  </div>
</div>
 
 

<div class="wrapper">
  <div id="slider">
    <div id="slide-wrapper" class="rounded clear"> 
        <form method="post" action="staff_register_action.jsp" name="sreg" class="form-group" id="streg">
            <div style="display:block;height:100px;">
                <div class="col-md-6 left">
                    <label class="form-control bg-transp">Lecturer Name</label>
                    <input class="form-control" type="text" name="firstname" required/>
                </div>
                <div class="col-md-6 left">
                    <label class="form-control bg-transp">Qualification</label> 
                    <input class="form-control" type="text" name="qualification" required/>
                </div>
            </div>
            <div style="display:block;height:100px;">
                <div class="col-md-6">
                    <label class="form-control bg-transp">Course</label> 
                    
                    <select name="course" id="courseddown" required>
                        <% rs = stmt.executeQuery("select * from course"); 
                    while(rs.next()) {%>
                        <option value="<%=rs.getString("cid")%>"><%=rs.getString("cname")%></option>
                        <% } rs.beforeFirst(); rs.first();%>
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="form-control bg-transp">Subject</label> 
                    <input type="text" name="subject" class="form-control"/>
                </div>
            </div>
            <div style="display:block;height:100px;">
                <div class="col-md-6 left">
                    <label class="form-control bg-transp">Password</label>
                    <input class="form-control" type="password" name="password" id="spwd" required/>
                </div>
                <div class="col-md-6 left">
                    <label class="form-control bg-transp">Confirm Password</label> 
                    <input class="form-control" type="password" name="passwd" id="scpwd" required/>
                </div>
            </div>
            
                    <div id="pwdError" style="color:red; display:none;">Passwords not matched</div>
                    <button type="submit">Register</button>
                    Already had an account? <a onclick="document.getElementById('staff_login').style.display='block'">Login Here.</a>
                    <Br/><br/>
        </form>
    </div>
  </div>
</div>
 

                    
                    <div id="staff_login" class="modal" > 
        <form class="modal-content animate" action="staff_login_action.jsp">
            <div class="imgcontainer">
                <span onclick="document.getElementById('staff_login').style.display='none'" class="close" title="Close Modal">&times;</span>
            </div>
            <center><h4 style="margin-top:35px;">Staff Login</h4></center>
            <div style="height:350px; margin:30p; padding: 30px;">
                Staff Username: <input tye="text" name="stid" class="form-control" placeholder="" required/><br/><br/>
                Password:<input type="password" name="passwd" class="form-control" placeholder="" required/>
            <button type="submit">Login</button>
            </div>
            
        </form>
    </div>
       <div id="admin_login" class="modal" > 
        <form class="modal-content animate" action="admin_home.jsp">
            <div class="imgcontainer">
                <span onclick="document.getElementById('admin_login').style.display='none'" class="close" title="Close Modal">&times;</span>
            </div>
            <center><h4 style="margin-top:35px;">Admin Login</h4></center>
            <div style="height:350px; margin:30p; padding: 30px;">
                Admin Username: <input tye="text" class="form-control" placeholder="admin" required/><br/><br/>
                Password:<input type="password" class="form-control" placeholder="admin" required/>
            <button type="submit">Login</button>
            </div>
            
        </form>
    </div>         
<script src="layout/scripts/jquery.min.js"></script> 
<script src="layout/scripts/jquery.fitvids.min.js"></script> 
<script src="layout/scripts/jquery.mobilemenu.js"></script> 
<script src="layout/scripts/tabslet/jquery.tabslet.min.js"></script>

</body>
</html>

<%
}
catch(Exception e) {
    e.printStackTrace();
}
%>