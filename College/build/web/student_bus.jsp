<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<%@page import="com.dbcon.*"%>
<%@page import="java.sql.*"%>
<%
    try {
        Connection con = Databasecon.getconnection();
        Connection con2 = Databasecon.getconnection();
        Statement stmt = con.createStatement();
        Statement stmt2 = con2.createStatement();
        ResultSet rs = null, rs2 = null;
        if (request.getParameter("busid") != null) {
            rs = stmt.executeQuery("select * from bus where service_no = '" + request.getParameter("busid") + "'");
            rs.next();
        }
%>
<!DOCTYPE html>

<html>
    <head>
        <title>COLLEGE PROCLAMATION SYSTEM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

        <script>
            function redirectWithBus (busId) {
                if(busId !=null) {
                    window.open("/College/student_bus.jsp?busid="+busId, "_self");
                }
            }
            
        </script>

    </head>

    <body id="top">

        <% if(request.getParameter("st_id")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;">Registered Successfully. Please Keep this Username for further usage: <span style="font-weight:bold">ST0000<%=request.getParameter("st_id")%></span></h3></center></div>
        <% } else if(request.getParameter("msg")!=null) { %>
        <div><center><h3 style="color:red;margin-top:15px; background:khaki; padding:4px; border-radius:5px; border:1px solid gray;"><%=request.getParameter("msg")%></h3></center></div>
        <% } %>

        <div class="wrapper row0">
            <div id="topbar" class="clear"> 

                <nav>
                    <ul>
                        <li style="padding:3px; background-color: khaki;border-radius: 5px;"><%="Welcome " + session.getAttribute("st_name") + ""%></li>
                        <li><a href="/College/index.jsp">Logout</a></li>

                        <!--<li><a href="admin_home.jsp">Admin Login</a></li>-->
                    </ul>
                </nav>

            </div>
        </div>



        <div class="wrapper row1">
            <header id="header" class="clear"> 

                <div id="logo" class="fl_left">
                    <h1><a href="index.html">COLLEGE PROCLAMATION SYSTEM</a></h1>
                    <p>One solution for college management</p>
                </div>
                <!--<div class="fl_right">
                  <form class="clear" method="post" action="#">
                    <fieldset>
                      <legend>Search:</legend>
                      <input type="text" value="" placeholder="Search Here">
                      <button class="fa fa-search" type="submit" title="Search"><em>Search</em></button>
                    </fieldset>
                  </form>
                </div>-->

            </header>
        </div>



        <div class="wrapper row2">
            <div class="rounded">
                <nav id="mainav" class="clear"> 

                    <ul class="clear">
                        <li><h3 style="    margin: 0 0 0px 0 !important;"><a href="/College/student_dashboard.jsp">Notifications</a></h3></li>
                        <li><h3 style="    margin: 0 0 0px 0 !important;"><a href="/College/student_bus.jsp">Travel Management (Bus booking)</a></h3></li> 
                        <li><h3 style="    margin: 0 0 0px 0 !important;"><a href="/College/viewstudent.jsp?id=<%=session.getAttribute("st_id")%>">Hall Ticket</a></h3></li>
                    
                    </ul>

                </nav>
            </div>
        </div>

        <div class="wrapper">
            <div id="slider">
                <div id="slide-wrapper" class="rounded clear"> 

                    <%
                        if (request.getParameter("busid") == null) {
                            rs2 = stmt2.executeQuery("select * from bus");
                    %>
                    <div>
                        <h2><center>Please Select a bus route to book seat</center></h2>
                        <select class="form-control" onchange="redirectWithBus(this.value)" id="bus_selection" name="bus_selection">
                            <option value=null>--Select--</option>
                            <% while (rs2.next()) {%>
                            <option value="<%=rs2.getString("service_no")%>"><%=rs2.getString("from_route") + " to " + rs2.getString("to_route") + " (serv no:" + rs2.getString("service_no") + ")"%></option>
                            <% }%>
                        </select>
                    </div>
                    <%
                    } else {
                    %>    
                    
          
                    <div>
                        <button class="btn btn-default" style="float:right" onclick="window.open('/College/sel_bus.jsp?busid=<%=request.getParameter("busid").toString()%>&seat='+(Number(window.localStorage.getItem('bus_selctd'))+1), '_self')">Agree</button>
                    </div>          
                    <% }%>
                </div>

                <!--<div id="1_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">1</div><div id="1_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell unavailable">2</div><div class="seatCharts-cell seatCharts-space"></div><div id="1_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">3</div><div id="1_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">4</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">2</div><div id="2_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">5</div><div id="2_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">6</div><div class="seatCharts-cell seatCharts-space"></div><div id="2_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">7</div><div id="2_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available">8</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">3</div><div id="3_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">9</div><div id="3_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">10</div><div class="seatCharts-cell seatCharts-space"></div><div id="3_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">11</div><div id="3_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">12</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">4</div><div id="4_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class unavailable">13</div><div id="4_2" role="checkbox" aria-checked="true" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class ">14</div><div class="seatCharts-cell seatCharts-space"></div><div id="4_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">15</div><div id="4_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">16</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">5</div><div id="5_1" role="checkbox" aria-checked="true" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class ">17</div><div id="5_2" role="checkbox" aria-checked="true" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class ">18</div><div class="seatCharts-cell seatCharts-space"></div><div class="seatCharts-cell seatCharts-space"></div><div class="seatCharts-cell seatCharts-space"></div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">6</div><div id="6_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">19</div><div id="6_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available economy-class">20</div><div class="seatCharts-cell seatCharts-space"></div><div id="6_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">21</div><div id="6_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">22</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">7</div><div id="7_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class unavailable">23</div><div id="7_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class unavailable">24</div><div class="seatCharts-cell seatCharts-space"></div><div id="7_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">25</div><div id="7_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available economy-class">26</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">8</div><div id="8_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available economy-class">27</div><div id="8_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available economy-class">28</div><div class="seatCharts-cell seatCharts-space"></div><div id="8_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">29</div><div id="8_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">30</div></div><div class="seatCharts-row"><div class="seatCharts-cell seatCharts-space">9</div><div id="9_1" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell available economy-class">31</div><div id="9_2" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">32</div><div id="9_3" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">33</div><div id="9_4" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">34</div><div id="9_5" role="checkbox" aria-checked="false" focusable="true" tabindex="-1" class="seatCharts-seat seatCharts-cell economy-class available">35</div></div></div>-->
                <script src="layout/scripts/jquery.min.js"></script> 
                <script src="layout/scripts/jquery.fitvids.min.js"></script> 
                <script src="layout/scripts/jquery.mobilemenu.js"></script> 
                <script src="layout/scripts/tabslet/jquery.tabslet.min.js"></script>
                <script src="layout/scripts/jquery.min.js"></script>
                <%
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                %>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>