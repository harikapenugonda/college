<%@page import="com.dbcon.*"%>
<%@page import="java.sql.*"%>
<%
try{
    Connection con = Databasecon.getconnection();
    Connection con2 = Databasecon.getconnection();
    Statement stmt = con.createStatement();
    Statement stmt2 = con2.createStatement();
    ResultSet rs = null, rs2 = null;
       %>

<!DOCTYPE HTML>

<html>
    <head>
        <style>
            /* Full-width input fields */
            input[type=text], input[type=password] {
                width: 100%;
/*                padding: 12px 20px;*/
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            /* Set a style for all buttons */
            button {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
            }

            button:hover {
                opacity: 0.8;
            }

            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 40%;
                border-radius: 50%;
            }

            .container {
                padding: 16px;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 70%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
            to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
            to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
        </style>
        <title>University College of Engineering Kakinada (Autonomous)</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="stylesheet" href="assets/css/main.css" />
        </head>
    <body>

 <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
   
    <script src="assets/js/main.js"></script>
    <script>
        function resetForm(formname) {
            $('#service_no').val('');
            $('#registration').val('');
            $('#no_of_seats').val('');
            $('#from_route').val('');
            $('#to_route').val('');
            $('#via').val('');
            $('#depot').val('');
            $('#destination').val('');
            document.getElementById('bus_error').innerHTML = '';
        }
        
        function validate() {
            document.getElementById('bus_error').innerHTML = '';
            if($('#service_no').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter Bus Service Number';
            else if($('#registration').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter Bus Registration Number';
            else if($('#no_of_seats').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter Number Of seats';
            else if($('#from_route').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter Bus from-route';
            else if($('#to_route').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter Bus to-route';
            else if($('#driver_name').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter driver name';
            else if($('#driver_contact').val()=='') document.getElementById('bus_error').innerHTML = 'Please Enter driver contact details';
            else document.getElementById('bus_error').innerHTML = '';
        }
        
        function printIt(){
  alert('hai');
  var options = {
  };
  var pdf = new jsPDF('p', 'pt', 'a4');
  pdf.addHTML($("#content"), 15, 15, options, function() {
    pdf.save('pageContent.pdf');
  });
};
    </script>
</body>



<div id="id01" class="modal">

    <form class="modal-content animate" action="#">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div style="width:90%;padding-left:20px">
            <label><b>RollNumber</b></label>
            <input type="hidden"  name="pageAction" value="login" required>
            <input type="text" placeholder="Enter Roll Number" name="rollnumber" required>

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="passwd" required>

            <button type="submit">Login</button>
        </div>

        <div  style="width:90%;padding-left:20px" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
    </form>
</div>

<div id="id03" class="modal">

    <form class="modal-content animate" action="#">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div style="width:90%;padding-left:20px">
            <span id="msg"></span>
        </div>

        <div  style="width:90%;padding-left:20px" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('id03').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
    </form>
</div>
<script>
    function showmsg(msg){
        document.getElementById("msg").innerHTML=msg;
        document.getElementById('id03').style.display='block';
    }
</script>


<div id="id02" class="modal" > 
    <form class="modal-content animate" action="addbus.jsp">
        
        <div class="imgcontainer">
            <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
            <h2 style="color:rgba(25,25,25)">ADD VEHICLE</h2>
        </div>

        <div class="container">

        <!-- SmartWizard html -->
        <div id="smartwizard" class="sw-main sw-theme-arrows" style="color:rgba(50,50,50);background-color: rgba(200,200,200,0.4);margin-bottom:16px;">
            <ul class="nav nav-tabs step-anchor">
                <li id="step-1-li" class="active" onclick="$(this).addClass('active');$('#step-2-li').removeClass('active');
                    $('#step-3-li').removeClass('active');">
                    <a onclick="document.getElementById('step-1').style.display='block';
                        document.getElementById('step-2').style.display='none';
                        document.getElementById('step-3').style.display='none';
                        document.getElementById('stp1btn').style.display='block';
                       document.getElementById('stp2btn').style.display='none';
                      document.getElementById('stp3btn').style.display='none';">
                        Step 1<br>
                        <small>Bus Details</small>
                    </a></li>
                <li id="step-2-li" onclick="$(this).addClass('active');$('#step-1-li').removeClass('active');
                    $('#step-3-li').removeClass('active');">
                    <a onclick="document.getElementById('step-1').style.display='none';
                        document.getElementById('step-2').style.display='block';
                        document.getElementById('step-3').style.display='none';
                       document.getElementById('stp1btn').style.display='none';
                       document.getElementById('stp2btn').style.display='block';
                      document.getElementById('stp3btn').style.display='none';"
                      style="margin-left:26px !important">
                        Step 2<br>
                        <small>Route Details</small>
                    </a></li>
                <li id="step-3-li" onclick="$(this).addClass('active');$('#step-2-li').removeClass('active');
                    $('#step-1-li').removeClass('active');">
                    <a  onclick="document.getElementById('step-1').style.display='none';
                        document.getElementById('step-2').style.display='none';
                        document.getElementById('step-3').style.display='block';
                        document.getElementById('stp1btn').style.display='none';
                       document.getElementById('stp2btn').style.display='none';
                      document.getElementById('stp3btn').style.display='block';">
                        Step 3<br>
                        <small>Driver Details &nbsp; &nbsp; &nbsp; &nbsp; </small>
                    </a></li>
                    
<!--                <li><a href="#step-4" data-content-url="./ajaxtest.php">Step 4<br><small>This is tab's description</small></a></li>-->
            </ul>
            
            <div class="sw-container tab-content" style="min-height: 123px;">
                <div class="form-inline">
                    <form class="form-inline" id="newbus" action="newbus.jsp">
                        <div id="step-1" class="step-content" style="display: block;margin:10px;padding:10px;">
                            <div class="form-group">
                                <label for="service_no">Service Number:</label>
                                <input type="text" class="form-control" id="service_no" name="service_no" required>
                            </div>
                            <div class="form-group">
                                <label for="registration">Registration Number</label>
                                <input type="text" class="form-control" id="registration" name="registration" required>
                            </div>
                            <br/>
                            <div class="form-group">
                                <label for="bus_type">Bus Type</label>
                                <select type="text" name="bus_type" style="width:350px;border:1px solid #ccc" class="form-control" id="bustyp">
                                    <option value="mini">Mini</option>
                                    <option value="med">Medium</option>
                                    <option value="large">Large</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="no_of_seats">No of seats</label>
                                <input type="number" class="form-control" id="no_of_seats" name="no_of_seats" required>
                            </div>
                            
                        </div>
                        
                        <div id="step-2" class="step-content">
                            <div class="form-group">
                                <label for="from_route">Route From:</label>
                                <input type="text" class="form-control" id="from_route" name="from_route" required>
                            </div>
                            <div class="form-group">
                                <label for="to_route">Route To:</label>
                                <input type="text" class="form-control" id="to_route" name="to_route" required>
                            </div>
                            <br/>
                            <div class="form-group">
                                <label for="via">Via:(optional)</label>
                                <input type="text" class="form-control" id="via" name="via">
                            </div>
                            <div class="form-group">
                                <label for="depot">Depot</label>
                                <input type="text" class="form-control" id="depot" name="depot" value="college">
                            </div>
                            <br/>
                            <div class="form-group">
                                <label for="destination">Destination</label>
                                <input type="text" class="form-control" id="destination" name="destination" value="college" >
                            </div>
                        </div>
                        
                        <div id="step-3" class="step-content">
                            <div class="form-group">
                                <label for="driver_name">Driver Name:</label>
                                <input type="text" class="form-control" id="driver_name" name="driver_name" required>
                            </div>
                            
                            <br/>
                            <div class="form-group">
                                <label for="driver_contact">Driver Contact:</label>
                                <textarea cols="250" rows="7" class="form-group" name="driver_contact" style="width:100% !important;border:1px solid rgba(0,0,0,0.45);padding:20px;"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <br/>
                <h4 id="bus_error"></h4>
                <button type="button" class="btn btn-primary pull-right" style="width:13%;margin-right:30px;" id="stp1btn" onclick="
                        document.getElementById('step-1').style.display='none';
                        document.getElementById('step-2').style.display='block';
                        document.getElementById('step-3').style.display='none';
                    $('#step-1-li').removeClass('active');$('#step-2-li').addClass('active');
                    $('#step-3-li').removeClass('active');
                    document.getElementById('stp1btn').style.display='none';
                document.getElementById('stp2btn').style.display='block';
            document.getElementById('stp3btn').style.display='none';">NEXT</button>
                <button type="button" class="btn btn-primary pull-right" style="width:13%;margin-right:30px;" id="stp2btn" onclick="
                        document.getElementById('step-1').style.display='none';
                        document.getElementById('step-2').style.display='none';
                        document.getElementById('step-3').style.display='block';
                    $('#step-1-li').removeClass('active');$('#step-2-li').removeClass('active');
                    $('#step-3-li').addClass('active');
                document.getElementById('stp1btn').style.display='none';
                document.getElementById('stp2btn').style.display='none';
            document.getElementById('stp3btn').style.display='block';">NEXT</button>
                <button type="submit" onclick="validate();" class="btn btn-success pull-right" style="width:13%;margin-right:30px;" id="stp3btn">FINISH</button>
                <br/>
            </div>
<!--            <nav class="navbar btn-toolbar sw-toolbar sw-toolbar-bottom"><div class="btn-group navbar-btn sw-btn-group-extra pull-right" role="group"><button class="btn btn-info">Finish</button><button class="btn btn-danger">Cancel</button></div><div class="btn-group navbar-btn sw-btn-group pull-right" role="group"><button class="btn btn-default sw-btn-prev disabled" type="button">Previous</button><button class="btn btn-default sw-btn-next" type="button">Next</button></div></nav>-->
        </div>
        
        
    </div>
    
    </form>
</div>


<div id="id031" class="modal" > 
    <form class="modal-content animate" action="index.jsp">
        <input type="hidden" name="pageAction" value="register"/>
        <div class="imgcontainer">
            <span onclick="document.getElementById('id031').style.display='none'" class="close" title="Close Modal">&times;</span>
            <h2 style="color:rgba(25,25,25)">ADD EVENT</h2>
        </div>

        <div class="container">

        <!-- SmartWizard html -->
        <div id="smartwizard" class="sw-main sw-theme-arrows" style="color:rgba(50,50,50);background-color: rgba(200,200,200,0.4);margin-bottom:16px;">
            <ul class="nav nav-tabs step-anchor">
                <li id="step-1-ev-li" class="active" onclick="$(this).addClass('active');$('#step-2-ev-li').removeClass('active');
                    ">
                    <a onclick="document.getElementById('step-ev-1').style.display='block';
                        document.getElementById('step-ev-2').style.display='none';
                        document.getElementById('stp1evbtn').style.display='block';
                       document.getElementById('stp2evbtn').style.display='none';">
                        Step 1<br>
                        <small>Event Time</small>
                    </a></li>
                <li id="step-2-ev-li" onclick="$(this).addClass('active');$('#step-1-ev-li').removeClass('active');">
                    <a style="margin-left:27px !important"onclick="document.getElementById('step-ev-1').style.display='none';
                        document.getElementById('step-ev-2').style.display='block';
                       document.getElementById('stp1evbtn').style.display='none';
                       document.getElementById('stp2evbtn').style.display='block';">
                        Step 2<br>
                        <small>Notify</small>
                    </a></li>
                   
<!--                <li><a href="#step-4" data-content-url="./ajaxtest.php">Step 4<br><small>This is tab's description</small></a></li>-->
            </ul>
            <img src="./images/event-time.png" style="margin-left:100px; height: 100px; width: 175px;top: 5px;position: absolute;"/>
            
            <div class="sw-container tab-content" style="min-height: 123px;">
                <div class="form-inline">
                    <form class="form-inline" action="">
                        <div id="step-ev-1" class="step-content" style="display: block;margin:10px;padding:10px;">
                            <div class="form-group">
                                <label for="email">Event Date</label>
                                <input type="text" class="form-control" id="email" placeholder="dd-mm-yyyy">
                            </div>
                            <div class="form-group">
                                <label for="email">Event Time</label>
                                <input type="text" class="form-control" id="email" placeholder="hh:mm">
                            </div>
                            
                            <br/>
                            <div class="form-group">
                                <label>Event Description:</label>
                                <textarea cols="250" rows="7" class="form-group" style="width:100% !important;border:1px solid rgba(0,0,0,0.45);padding:20px;"></textarea>
                            </div>
                        </div>
                        
                        <div id="step-ev-2" class="step-content">
                            <div class="form-group">
                                <label for="filee">Send Notifications to Students</label>
                                <input type="checkbox" name="mail"/> Mail
                                <input type="checkbox" name="Webno"/>Web notification
                            </div>
                            <br/>
                            <div class="form-group">
                                <label for="filee">Upload File</label>
                                <input type="file" name="file"/>
                            </div>
                        </div>
                    </form>
                </div>
                <br/>
                <button type="button" class="btn btn-primary pull-right" style="width:13%;margin-right:30px;" id="stp1evbtn" onclick="
                        document.getElementById('step-ev-1').style.display='none';
                        document.getElementById('step-ev-2').style.display='block';
                    $('#step-1-ev-li').removeClass('active');$('#step-2-ev-li').addClass('active');
                    document.getElementById('stp1evbtn').style.display='none';
                document.getElementById('stp2evbtn').style.display='block';">NEXT</button>
                <button type="button" class="btn btn-warning pull-right" style="width:13%;margin-right:30px;" id="stp2evbtn" onclick="
                        document.getElementById('step-ev-1').style.display='none';
                        
                    $('#step-1-ev-li').removeClass('active');$('#step-2-ev-li').removeClass('active');
                document.getElementById('stp1evbtn').style.display='none';">FINISH</button>
                <button type="submit" class="btn btn-success pull-right" style="width:13%;margin-right:30px;" id="stp3btn">FINISH</button>
                <br/>
            </div>
<!--            <nav class="navbar btn-toolbar sw-toolbar sw-toolbar-bottom"><div class="btn-group navbar-btn sw-btn-group-extra pull-right" role="group"><button class="btn btn-info">Finish</button><button class="btn btn-danger">Cancel</button></div><div class="btn-group navbar-btn sw-btn-group pull-right" role="group"><button class="btn btn-default sw-btn-prev disabled" type="button">Previous</button><button class="btn btn-default sw-btn-next" type="button">Next</button></div></nav>-->
        </div>
        
        
    </div>
    
    </form>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    
 
</script>

</html>
<script>
    <%
        if (request.getParameter("msg") != null) {
    %>
        alert('<%=request.getParameter("msg")%>');
    <%
        }
    %>
</script>
<!--
Template Name: Academic Education V2
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
<title>COLLEGE PROCLAMATION SYSTEM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="./layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
 
 

<div class="wrapper row0">
  <div id="topbar" class="clear"> 
    
    <nav>
      <ul>
          <li><h3 style="display:inline"><a href="#">Home</a></h3></li>
<!--        <li><a href="#">Contact Us</a></li>
        <li><a href="#">A - Z Index</a></li>
        <li><a href="#">Student Login</a></li>-->
        <li><h3 style="display:inline"><a href="#">Logout</a></h3></li>
      </ul>
    </nav>
     
  </div>
</div>
 
 

<div class="wrapper row1">
  <header id="header" class="clear"> 
    
    <div id="logo" class="fl_left">
      <h1><a href="./index.jsp">COLLEGE PROCLAMATION SYSTEM</a></h1>
      <p>One solution for college management</p>
    </div>
    <div class="fl_right">
      <form class="clear" method="post" action="#">
        <fieldset>
          <legend>Search:</legend>
          <input type="text" value="" placeholder="Search Here">
          <button class="fa fa-search" type="submit" title="Search"><em>Search</em></button>
        </fieldset>
      </form>
    </div>
     
  </header>
</div>
 
 

<div class="wrapper row2">
  <div class="rounded">
    <nav id="mainav" class="clear"> 
      
      <ul class="clear">
        <li><a href="./index.html">Home</a></li>
        <li class="active"><a class="drop" href="#" onclick="$('.active ul').css({'display':'block'})">Transport</a>
          <ul>
              <li onclick="$('#t-parent-div').css({'display':'block'});$('#e-parent-div').css({'display':'none'});
                  $('#ht-parent-div').css({'display':'none'});"><a>Vehicles List </a></li>
              <li><a onclick="document.getElementById('id02').style.display='block'; resetForm('newbus');">Add New Bus</a></li>
            <!--<li class=""><a href="portfolio.html">Create Alert</a></li>-->
          </ul>
        </li>
        <li><a class="drop" href="#">Events</a>
          <ul>
            <li><a onclick="$('#t-parent-div').css({'display':'none'});$('#e-parent-div').css({'display':'block'});
                  $('#ht-parent-div').css({'display':'none'});">Events List</a></li>
            <li><a onclick="document.getElementById('id031').style.display='block'" >Create Event</a></li>
          </ul>
        </li>
        <li><a class="drop" href="#">Hall Tickets</a>
          <ul>
            <li><a >Students List</a></li>
            <!--<li><a onclick="document.getElementById('id031').style.display='block'" >Create Event</a></li>-->
          </ul>
        </li>
        <li><a>Users</a>
        </li>
      </ul>
       
    </nav>
  </div>
</div>
 
 

<div class="wrapper row3">
    <div class="rounded" style="min-height:350px;">
    <main class="container clear"> 
      <!-- main body --> 
      
      <div id="portfolio">
          <ul id="e-parent-div" class="nospace clear">
            <li class="col-md-12">
                <h2 class="avmodule" style="display:inline;margin-top:5px;display: inline;top: 17px;position: relative;">Events</h2>
                <button class="btn btn-info pull-right" style="display:inline;width:15%;">+ Create Event</button>
                <table class="table table-responsive table-hovered ">
                    <thead>
                    <th>Event Title</th>
                    <th>Event Date</th>
                    <th>Event Time</th>
                    <!--<th>Max Payable Amount</th>-->
                    <th>Actions</th>
                    
                    </thead>
                    <tbody>
                        <%
                    rs2 = stmt2.executeQuery("select * from events");
                    while (rs2.next()) {
                        
                %>
                        <tr>
                            <td><%=rs2.getString("event_title")%></td>
                            <td><%=rs2.getString("date_")%></td>
                            <td><%=rs2.getString("time_")%></td>
                            <!--<td>N/A</td>--> 
                            <td><a href="deltevent.jsp?id=<%=rs2.getString("event_id")%>">Delete</a></td>
                            
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </li>
        </ul>
        <ul id="t-parent-div" class="nospace clear">
            <li class="col-md-12">
                <h2 class="avmodule" style="display:inline;margin-top:5px;display: inline;top: 17px;position: relative;">Vehicle List</h2>
                <button class="btn btn-info pull-right" style="display:inline;width:15%;">+ Add Bus</button>
                <table class="table table-responsive table-hovered avmodule">
                    <thead>
                    <th>Bus Service Number</th>
                    <th>Registration Number</th>
                    <th>Route - From</th>
                    <th>To</th>
                    <th>Driver Details</th>
                    <!--<th>Max Payable Amount</th>-->
                    <th>Action</th>
                    
                    </thead>
                    <tbody>
                        <%
                    rs = stmt.executeQuery("select * from bus");
                    while (rs.next()) {
                %>
                        <tr>
                            <td><%=rs.getString("service_no")%></td>
                            <td><%=rs.getString("registration")%></td>
                            <td><%=rs.getString("from_route")%></td>
                            <td><%=rs.getString("to_route")%></td>
                            <td><%=rs.getString("driver_name")%></td>
                            <!--<td>N/A</td>--> 
                            <td><a href="deltbus.jsp?id=<%=rs.getString("bus_id")%>">Delete</a></td>
                            
                        </tr>
                        <% } %>
                    </tbody>
                </table>
<!--                    <div id="content" style="background-color: white;">
                    <h3>Hello, this is a H3 tag</h3>
                    <p>a pararaph</p>
                    <img width=100 src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAlgCWAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCADIAMgDAREAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAAAwIEBQYHAQAICf/EAEEQAAIBAwMCBAQEBAIIBgMAAAECAwAEEQUGIRIxBxNBUSJhcYEIFDKRFSNCoTNSCRYkQ1NiscElcoKS0fCy4fH/xAAbAQADAQEBAQEAAAAAAAAAAAAAAQIDBAUGB//EACwRAAICAgICAgIBAwQDAAAAAAABAhEDEgQhEzFBUQUiFAZhcSMygbGRocH/2gAMAwEAAhEDEQA/AMqa4x61+sH5UD/MjOOofvSsVhElVuO9Owschl4xxQUggkTHegYsSLQVZ4Mme9BIsPHnuKBoKhRhwRQUGQp/mFBSPMB6GgGwDsBjkUEnBIvyoCxfmADvQFg3nX1xQNtCVmjIwSKBCuqM+ooA8TGfUUrAH0R+lMGhPQo5zQTQh5FHqKaYhpNedBIBpEsY3N7wcEGlZLZHvqB7HjFOybR5b3PrSsT7Gb651DHUalsLYhNWUnluaVjskIdUTIw9UgUhz/FBj/Ex96dlbiTquDxJn70WLc9/FyP6zRYbnv4yc46jRYtxaauM8ueaLHuPINXXpwXPf3ospTHcepo39dFlqYttVQKT1UxtqhpNqYJyGP70J0Tsgf8AEiD3NDYbI4+q4Ges0rFshlNrQHaQ0WTswa6yT2kNLZDTbCDW2A/WaTkmNNnRrucc1O6HbCLrXPLkCr3QWxTaymP8U0t0G32MZ9bGThzimpJkuQyn1sHjqzS3RDYxl1kH0/vQJsYzav8AFwO3zotIS7FQ6r7kilsil0QLaic4D1kpg4tB4blzg9Xfmq2QqY+S7dQD1UeQpxPSam6jlzQslir7A/xh846zVqSZL6CLqUr9mosVhBfzE8mixBRfScHNFjsLHqMn+bFHsaY9h1NsD4qYwrao3ScN/em2O2xtJq0g/qqHkob/ALAG1twfibFZyyWwSbG82utzhqXkK1Yxm1pz/XQ8gasQuryn+uo3sai0LGrzAY8zFJzopREnXJVbBkqdytEEXWm7+ZQslsNEKOtSf8TIq1JkuKAzam0g/XVKTRDiNvzj5wXo3J1DpKrd2zRvRSx9nW8s+tS8haxiQUU9/wC9T5CvGRCQ/HzzWKmavEh4g6QAPSq8jF40O4zI478UKTQ/GhLxu1Vs2RLGgZgINbIxnjQ4t4iKtNmdIexW+e4q0DSCG3A7VVE0JMXSaAOqJAeBSspIXmRhjHNKykgTxsf1H6VjJlJDaS2kA5FZs0SGU1q+TzSGB/IOTyamyqHUOm/DllzSGkjkmngc4qWOkMbi0cHgdqB0gIhn7AUKLRDHcFpK68ritopkMJ+SYDmrSM2mwLWxB+9S0ikdVWQZGeKxm2jaCTOtI475rKUmaqKE9Z9aSky1FFnuNqyBsqpB+lRHpHRKNsAduXC91+9VsZOFBl0hoxjpNND1aPNp7L3Q1aZLTBGx90NaRlSMpRtji309eCQRmtFMz0H6Wa4+KhZEHjYs2seOBVrIn6E8YJ7NDk47Ubi8f0R+qXVro9pJfXbqEQcAnBY+gFZZ+VDjx2bNsHBycjIor0Zfr25tb1WRwl4beFhlY4z0gY/6mvmOV+UzZek6X18H1vE/F8bAuo2/v5Kbq+nX9vD+cuppVUEkFpSCfpXmvkTu77PSjhVV8DvZvijqWgXS2uqzPe6YxCsH5eHPqpP9wa9Hg/l8mCWuV3F/+jzPyH4fFyY7Y1Uv+za4zbXtrFe2kyywTIJI3Xsynsa+shOOWCyRdpnx2THLDN45qmjqwnq7Gk2CHsdv1L2qLLOvaEjIFMQxubQn0qaGCjsVzjpOaEhNWSEVmEX4lyatC0ETwIBkLTtC0IydSGIxRaJ0BCJj2Q1EvRcfZ4WkspCqjZ+lZS69mqHMO3ruXnpYfLFZtovVs3jS9qNqZbMRx9K5Xko9tcfYFr+xm06LzUjJGOeKamTk41FYGkdbBUhLMfQCqeSjm8TfwTuneGl9qSCR42UdwMVLzG0eC5+xxc+E91bL1LEWP0q45uglwHEh5NoXMLlJLUjBxwK1WVUc8uNq6Pf6pyuOICKHOhfxZHRsq67+SQKSyg+LIr3iLom49C2lNqu37V5bmOZEaOO3aWToPcoB68Y+9eX+T/J5eJCoR9/P1/k9r8N+Cw/kN5Z51rXX2Ylvnel/uz8jFd7aGhQ6arRdEhYSyk92fPds/IV8ri5GXNJzyz2v/wAf8H1fO4/ExVDjw0S6a67/ALlU1y70M2ZjtdRnWdwQcqOkH510W/k4FFIst1uHw+vtoXtrqEc99dXVpBDZ2klmkX8NnXHmzi4DFpi2CAuAAGOewobLTSMhujBEJBFAVDPwpOQB7fOpvshqzZfAXcceq2s+0LtsTWoNxZg/1R5+Nfqp5xX0n4nmXB4pfB81+Z4SUlnivZstvtqZ3yUOPavY8lnirBZN2m1upeYTRuaRwMLNtFzHlIhTWQHx2Qd5tueJiPJ7GtFNUYSxSTG0e3rsv/hNj6UboSxSfQ8O3bsqMRMftR5EivBMC+19QkGVtG+9Q8sQWCbERbC1G4bLwsoqPMi1xZMkY/DW8A6sEge9HmRX8SSH+n7HdZMSQYx7ioeRMuHGlfZaLPY6kA9A59MVDkjshxfk+ltB8OrW1jZPIAOfavP8h9BHFFDXdex7eW1MPlDng8U92E8cWUvRvDSCO8B8hSM+1U5ujnjx42aXpexIkiUGEAADsKhzSOpYVQ4vtjxmM4iGAPahZUweJeil6lsOPziPIB5PpW6mYTwRGSbGTrB8gY+lXsiVgQWXZShcCLGPlRsgeBDcbQKHKqR9OKltSi4v5EsKXZTN+/h82p4gWUsd5ZLa6gQRHeRIA3Uewcf1CuLPwcOW2lT/ALHRjnkj7d/5Pz+8TvDrUdjbmvduX9u1vc2sjL0kfCcdiD7HuK8GcZY5OMzuT29En4J+Cu+PG7dMW0drROEiw17fGPqitI/Vj/zH0XNZOauilFs+qt0f6MjSYtqSS6PvDUBrUcbMs1zgxSPjgMgX4Rn1rRJP0NwaPhxLTX/CjxGWy1WD8vqWg6gsdwqt8J6Ww4DdipGea1485YMql9M582NZYOMj9G7HZPnRRXEUA8uaNZUYc/Cyhh/YivrlNONnz0OM0+yXh2YUUfy+fpQpI08CFnaTuegRj9qe9FLAg6eHKz4MkIOflS8pX8SMuxUvhosI6hbjHrxUeUP4kV2ct9jIG6RbD9qHMa4yHEuwQSP5IGPlUPIV/HX0Fi2csYAaFfbtU7s0jgS+AzbQjVeIemjdg8Koj7vbKRnqVR+1G7IlhQbT9JQYBT1pqbKjj6PpS5aC3BCKB69q8+KbR6BDXcK37EdINaptEsVZ6DHG6uVXH0qHNtFxSssttbwRooYDgVi5Nmhy7NuUKLj9qEBB3OlwzMWwM1sptIhpMGmixD/d/uKt5GLRIRPo8RT9CihZGGiZFXOmRqCOkH7VcZWJwSIDXr/QNsWR1XXr2O0tw4QMw6mdj2RFHLN7KKcsyxdyI1v0RWvfhO21+ILXNIi35tS5sLyG3W6muIp2imtbR+Y4pscMzDnoP6c9+9fP87kx5Dc4qv8A6d2KHiVM+lvCb8OvhX4KaH/BNk6BHbxMxaWQrlpG92Pqfmc15ibNNvomdy6LZS20hSxLqBjpHAPp962hNplRbfs/PH8b34PIvEO6j3XtQWum6+g6XRl6YriP/K3T2I9GNdtLNHr2ZZY6Oym7an/Er+Gjb+jS+NmiW259leVHB+e09kll06HAAYyJgsFGOHXt2Nd/F5s+lM48mFR7o+nLOHTtT0+11TTbiO5tL2FLi3mT9MkbjKsPsa9eM9laMNUHttJjeTPQuaG3Q9UTltoydP6R+1Z7mih0PH0eDoIkUY+lRsx6DJdJtUdiFU/ajZhqgctlBnhRRbCkMbi3gRsMoIothSG1zDF5ZCgZqrIa6IS4ijBPWBjNFsmkBiiiD5QCqTYn0a5qDlpiFPFc8OkaSXY40626fjPrzU5Gi4JklLII0yo5rJK3RqRlxqpiJBbFXoJtDT+IGZs9Q9qpRaJsMt4keOog1LTQ12SVvMZlDKvBqHJF0euZo4kzIB+9OPsKZXdSvrQnAI6jxjNdC9GcvZnP4dtPXxx/EnrO59ViS5214fx+Vp9rJzGbzOBKVPBbIZs44wteL+RybPUvH2fcMVpBay3EyRgSXUnmSN6scY5ryjYHdzOF+A0FRX2Quq3C+URcPgAcfKqijaMTMdc27FuTW1EkqNbxMWc5/VXSpUqNVHog/E20sl29Po72cMts8TRPDIgKshGCD8q2wP8AZGOX/YzD/Ck2mibXO0reQeVolzJDbof93bM3VGo+QyVH0r6LGv0R48ZK2i8QzfEGHerplkxb6qqKOrjArNwbZVoRcayHBAYfWhYxOVjFtU79T/tVaAmhvLqER5EhJpa0DaGj3ayHhu1Ajj3fw4c9/WhAyMu2RxwaokVZqi/E3eiwNPkliab4uOfWudI1TRKwNEsQII7VnLtmifQ2vrzpQ4PFVFdg/RW7q5MrnkitlFmbZGTamYn6I+on5UpfqrYk7dFq2poF5qzrPICQeea8/LyUrOqOOjRBtcW9r+gZArh/kI1UOzLN/wCqPokcpc4C/OujHyF8jnjpWYlP4mwzXNz0XORbq0jYPsCcV0/yFVI4pPs1P/R0aU1vsDcW8ZISp3FrcxRiOSkYx/8AkxryeZK2aYY/q2fYDdEnc81wtmyI3U3MMJaJepifftSujSCtlD1oahqEpjaQorcEMauM1R1RiqIFLePSZsI56yeliTkE1WwygeIurLcqy5BHqoPtXVifaMMi6ZiW2njstx36rkG6jB78fCSf+9fScealCjwpfpkb+y/Wd58AJPetrNEw73ZIwposBlcTyr2Y0tkOhr+ZlbPxGjZBQBppWbuaTkIXHcOpAOakpB5puteDigGNHkkJ6RQ3YqHFt1v34pWNIvcsshkHmZBHtRQmPoL9ulVye2KhxVlqTR29mLRE55pKNOy3Kyq6hfPEWKg5H961iyGO9saedSulkmUnntXDycvwdOGCN+2fpdvBaqBEBgCvDzTbZ1VRZL6OCK2YsBkCue2EfZ8nfiO1WOO0uxCekgEDB+VNyaNci/U+Idj69d6lq2t6dPdsHbzeo9+mMLkn/wC+praGRxdnB4lKTP0x/B9pLaB+HzaEL2r2z3Vs980cgwyGVy3xfPBFLPN5P2ZcY6qjaxfRxjLmuVmtWQ+vaykMDeURkdzUNm2PGUe910mRumQN0+pHrStnUoUVbU9cunbkjHV39iae7HqjMd6X0kqMwKk55OK7MU7ObLGl0ZHrzarb29xqGiD/AMQtUae3X/iOnxBD8mwV+9e3xszUWeJmhcrLlsffm3d97YsN1aHcK1tfR9TIT8cEo4kif2ZGBB+ld8MqmrQtaJ83lvn9S/Y1WwUJe6hZSCRSstobtNbrn4hRZI3N1Cp/UKLCgcl5B36wKdoAbahCB+vtSsVgm1a3jGSR+9JyoLQ3k3HFEpKuP3qXIe1H0bdbKEhL28uWP7ViuR12avH2MV2bqaydJVcD1qv5CJ1HLbK1F1P8rqFR50ytSGvth3rMS1kxwaayOXQ1H7H23dFFhOOuMpg8g1zZ432bQepodjrcOnw5ZvSvLyY3Z0xdkPuDxAheJoY5PiI9K5TohjXs+aPGH83rVtckc5BPNL2GWNro+XdJ2febP03W9wG2eXUtTvLayVeknyTPII4Ex6lizSH2Cr70nKmkc8IPVv5P1h2naxaPtbStIgTC2dnDCo/8qAH+4rWYKB26u7huoHJGKwZqoqyr63euIiHckZGRWLbOmCRULqe4eKYxAmRvhUe+O/3qW2bRSaIW8E4UPNKT1HqJ+XpStiaKJrKyShlf4lLcE+tbYpOznyK0Va6sVicuiAEc17HGlUkeXnhSbMe0y3l8Jd4Xs9orLtrc95/tEa/psNQb9MnySUfCf+YCnwubrnlhl9k5sNY1kL0m+I+rpMnbivb7R53lDnfNvjiYUm2PyjeXfMZ7TA49qlzaDyWMpt/QqcGSoeRhuM237AxP+0Dg9s01kJeWjzb4jkGA4GatzFuMbreSHvL/AHrN5GG5BX2+OjIWXP3rOWR2G5+lwtGVehTiuZyPUFRWN2OcmlvFfJNDhYbtV/XzS2QJA2jusYZGP2o2RQ1udKmlbrEbAkYzirjkSQAf9XWYEyzEg/00OcfoCOk8PtFku1u5oHHuOrjNYPHC9q7NPNkaqxvuvRNpbT2zqW4p9KgunhixHHKM+bK3CIB7lsfbNKWTxRckqHj2yTSZmeyfDrRJtW0fb2r2SXD2kja1esf0y35II+oQ4UD2UV805yyZ1L7PccIxxa32fRFuUEAVlIKnORXpN27PMaI/UJn8osrcjj6CsJezaBUdXi6iTMxw2OBXPLtnVEgbkyfBGD0hW6ifU0qZRD3zAr5Y+L0waaAqWqRSeaMp8ua3gYZFfREahZZh68AEZ7V6PHa2ODNHrop2tbWS+tJUvrYvZXwMRb061wf3GQa8/kRnHL5V6OjDrOHjkZvubwr3ta3N9JokDX0Vq6h44v1gMvUrAeoIzivqOFyPNhv5R87zuFPHkco+jO9S/wBZNKumg1OwubR19JYitbSnXs4Y45X6EJqV+4GZPTI4rmlmS9G8McvlDK+uNSf9LsPTFYPJbNPExrANSLZMj5rRT6M3HslY3uxHgy/tVPIqEoMT03cx6RITWLmWoNji30Oab4pATzTUkx6H68rbqjdQjyRXO5WeoFKtMMFVGPlS3QC0s52HUVWpeRALls7hF6vMB+WKFMBqbeefjgkVpukACW2nXOVFPZAR0q3KPwM57e1abIDIfEK91DdHiztrZFvHINP0SBtc1HB+GSY/Bbo30OWxXHzm/wBccfk7OGqbm/guuh6db2m8vy8a4MVmFZx3ZzyzH7muHlYVhnBL4R14su6lJ/ZdpWZGKrmiMqVMxojbrzDlSpPpk1nJ2aQRAXlpjLytkJk1kzoiyC1CJmk4AGM4pFWQc8PlAEjJzjNOhld1bLSt0p//AGtYHPJkXMhli6SveuqEtWYyjZYdo7Zl3HtnWNES2WRoXW6gYjlJMHt9cYrohBZcckzGdwkpIRsuKODWtP1GaJVW4Q6Tdqw7PnML/UHqT/1Cp/H5niyav0bcrGssNol41rw+0Ld6TWuoaXbyxlSp8yIZH3xmvZ3jXZ5UoKR8975/BzfwXb3ezJ1aNic283AUfI1hkx79xZPjKNe/hb8TbaVbdNFiuMp19cb8A+3PrWXjcX2xPGyhbn8MN67Q6n1rat9bxocGQRFl+uRVUYTxtMpU90Q/QqMOcHIwf2pP0ZU0yS0tephkDJxWRpFFrtbVSoPSKpOjRJUfqvmPHTkVzdo60rOrLax8MM47/WppiHUEscmSDwKTi0AKWYsWYAdIqkq7AbxpKh8wEAGq2AHN8ROXOc1SYDdoFYdIZRn+o+nzqrfwBmexdMh3Huvcm7n5jurowxP7xxfAgHyyCa50/Jm2+jvyPxYVBfJLaZEI963wU5CpgH7VHOTclIOM6x0W6RgcHoHNchoyOu4yeA+COTUM0gV+/wCroeMHqzwTSqzS6ITUDgDHtTSodkLJG8kLEqM5ooNiA1G2fzmYL61pBdmT7I94OhcsMVvRLVmh+DylJtUYgdPlxH75NdXH6t/Zycj3Qw3pt2W11PUbaxHlDV7c3FrxgJdIc5Ht8WDWGfHpLaJvgn5I6s0rZ1+m7NvaZr7IqPeWqPIoGMS4w4P0YGuxZXKKZwyhrLUmm0mMuFZxg+oNPysloRdaLGsYEU2CPU4qllfyIj7vQYbmAxXXlTKR8SuoIP2NPyj1TXZnO6Pw/eHm8opl1Ha9osjDAngTy3B98itHJPozcEzLR+CDbUM7GDdmoQqSSiNErBfuRUaxF4kVzdf4Vd4aBbSXGgalDqqR8+X0dEhH/Sk4P4Dxo+xXluFbKz9QzzzUpRfdGlixeyhgWiPA5+tLQB/ZXqdOXOOqspRbKaONqMUbMmGZc8AU1BtEg5r53VVgDAZ5zT0r2AtFuGw3Sv8A6qQFf8SdWudsbE1vXPNCyQ2pSBF7tLIRGg/9zCk5qKsvHHaaQDw+2y2hbSsLQE5Eal8e/qT/AHrPjKo7G/JleShnYMtrvW/tnjK84Ut/UCMg1fLW0FIMMqVFnJLKfl615xvdkZeFQrFT9TWbNoEHKrO5x+k0JWy2R11ahmA+wrXVCsbjTwIsEZ49KQWQt9pg8xgFz7fOtYpEv0RF7piRLl5snP6faqIZcvCiBurUJY1YoBGpx2zya7OOv1OTP7LPvWyS80Rb2GI/mdLcXKn1Kj9Y/Y/2pZ4toMEqkkhh4S6gkGnazo4XqjsdUdoR/linQS4+gZm/eowdw7Dk9T6LjHL0SMFBA7it2lRzBmd5OSSaVFICZWJKeWCBxmlQrBhwjc/Cx9PerY6EXJjYdSsM+5pxYUMjP0nplwVPpVU2BMNYSBepMnFZ72SA6GBKyhsVeyoAwjgCD9RqdmAorCy4BII96Sv5ADGrI+QgYU7AdrK+eR0/MetSwM/8b7q4vbLaW3CP5Wr7iiMp/wAyQRPKB/7gp+1c+df6b/4/7Onjf70aLa2sttaQwCIfy1C8+wFaJqjCXtlbvLZrvecQMeOm3zgfU08jXiNMTomJoCFxjBHc+/yrzzf36IHUmPUVULj2FDN49EYerzCCp7DNTRp7OiFSw9vWtV2RL2eeFAuAOfQ0AiOvrVT8ZHSB600UVjVIoyzdTEgf3q4q2S+l2XHwig6bLU5pHVEMqIBnuQD/APNdcY1GjgytbF6uJrR0a3c9SsCpBXggjFPV0ZX3ZSNs7cl23NrJknjkW/u1kg6fSJECqD7Hv+1GHFo2zTJk8lNk0s4UENJjPat32RZJWssoQCOQMe/NZSj2FhWumB+OEZ9SvrRqiRvKGuMlEINNAho9pcA9RZgPp3q018jbBTSSRctCOKpJP0IsoedG6SCwPrXIAkr1jq6GHvxTsAi+Wy/AnPzpKwBuYw3S6oc1fYCM268eWQc/00f5AUkIc9XRIfWk2gK/unad1uPW9p34jYw6FqUt7L1ccNAyAD3+Iisp01RcJaeizz3V4mT5fFaJRfon4Iu2VZdwrdPGy5gKc9+M/wDzU5o3jpFR9EnqZjWIj/pXFRvCylanP5chBPOeaVHUlY2EhMfV0sCaKLSoV1kKDzmqXSJl7FZ5HT680DS6Gd2rSKYyp55p1QL2VnULXDkNn1Na4/YsnoumwtIuLXRBKV6fzUplGTglcYH/AErtTSXZ5s+2TF9Kscn5diQ3oc96tJNWiaAieGUdMjqQn6ulgSPqPSqUWIBPdaSAVMoUgZJfgVSi36D17OQSW8aebBcBwe3TIMUnB/IWgw1YrgM6cD/NQsdguzqagz/4TDq/tScUgHP5666CC6KR7A0qQAJhPcklmD8dhVKkBY1cOCBJjPbNcYBEu4QpRiOocVLj2B6O3E3IbpHsKe1AOF0+AqXI5pbsDv5WJAMD580nJsAsaoMVLsAjSEYUAAfvUtf3ABcr1fp7GqjKhpWRc9u8TC6Ucxnn6VblsqKXQK9vEaHkCudwpm+MqGolPzJlc/COT7VEulZ241t0DS9tnB6GBFZ736Nnjo486P2GKpSJcBSXVunMkgAHfNG6GsUmNzqFvdFvKdSB65ojkUgeJrsqW9d0aZoVhJJcSBmIwFAzlvTivU4eB5GcHJzLGqZnaePD21yk82qXEIgXHko3wccBQvpXtrhxao8d8qKITdv4kdz7gsmsLGOGxjYFXlj/AMSQehOf0n6V0Y+Bjh2c2TnSl0qM6s94bis7t7yz1m9jllPxuszZb68811PFja7RyxzZE/ZNRb93ReShr3U7mc4AyznOKl4ca9I1WefyWrRN6bmbFrZXU6K55HWec1nPFD20dOPLN9Gz7BsdW/L/AJjUIJW81erLMWNedncE6R1wv2XNUEbgxcr7n3rkfaNR/bTOW6c4J9DWdAOngYnMTYJ74PrU2gJhfKAKt7VzMDwKq46QDkZp/ADuIiNQzMQD6VDXQDiK6iAIz3qGqA60oJz/AE0gBG6Cv0qvFVqwOG5kz2o1sD35wAYcY9qWg0NJ9QVlKuMLg/erSooY3dm76UbuNh2/Y1lKSto3xGaa/q16J5NMFufOXlz6dNc2RWqPVxJJ2NdNuZI4/wCaCKzhHX2aydjmfWYIFJMwGKcuvQ4pN9lH3fv+302Jj+YUZJyfWs4YsmZvU2yZcWBLYi7PfVnp2lx389+v8/AVc+5/7Vmm8fs3hxnminEy/wAatz3Fzf2FvaXp5jM79J75/TX2X4hf6WzPh/zjlDLpFlC0yy1fVi0tujz9JwyqMkmvZWSMTxI4ZZOy4aV4V7y1G4htE0pxJMhkTzWCjA+dRPkJHRDhzfwW7TfAvdjySxNo7BoY1Zizggk+gx6/KsXzMf2bx4bstOg/h43HdI0t7DHY9BGBMSS49xj0rLJz4RSo1jxPs0PangeNLnE2oHq7FDGDzj61y5PyG3UTohgUUafbWb2UAt4LcoiDpGV9K4Zyc3bNV0Bm0WW5BaJAo/7/ACpeT7AANKvghMkgBHb3qlJMBC3F/bcFOvp/+96HGLAm0IUg5JbtzWI2FDsx4XGfQVDEEj88cBsigAnS45LikwCYZELOeCO+f+1KgOGRXOVcN86RQSKQ8dX70P0KhcsClQcZJqVJIEBNl1j9Ax86fkQykeKl1rembc/IaHH1S3dwqHIz0jHJ49K5eXJJJv5Pb/C4FnzU/gzDbO4p9TS50/WLiGy1q3VEtQ3wNdpzllVuTjAzj61PBzxnPxN9nf8Al+JLAtoLr5A3ut6zKfJkihOBgSL8OT9K9OWOEumeFGbTK7q9juq9tvPtppF6gTgoRke496w8MPgqUpNGWbk0fc15GVjsrm8lY8KinnHrn0wKWXP/AAsTnDpi4/ElzeRHHNWiT0Lw833ujSbfU9P07T3tGAEMsl8uG9OsBATgc96+U/kyb3fyfo8eLGGNY18EhL4TWs2hHcx3RYarFaxn8w1uGkAKnDqM8/Ce/btX1HG/qFYMShOPS+T4rl/0rLkzeXHO/wCxs3hT4OWthZ2Op2V7aXtnNGJIpok4kB5//X2r0Zc5Z4qcfk8B8Z8VvFL2ja7bbdpGqN+VgDxqQpVBkZrleXsgcLpUUQ6ljAB749aXksB/a2MCgYFZykwHbLHH2x2+9TdgNpPKeMgsQPWgBofy6SdXnAkDGM1tbYA5JIXwijqOeTTXQUDnsIOjLj7U1OgEPag8xjFCf2Ng2Toz5mRgcUrCjyLIHBUkYz3osR5fMkbLkdI9M0MdBeR8LDOexHtSTCgkduGx0gZ980DOFZMcZ6qQwsdzPEvxDJ+dJxTA9JfzKD8Iwe9LVBRnXiRrtrFbpZ3wnkFxOsKrFIUbLHHDDkcc15P5LL+0cS7Z9l/TfDlKMsv9v+ij6hsjZunapHImkyXszDzIZbi5kdxKoyOhmbg15EJPHnTSPpJxefA/JVfRzZWmbpu/EOW+bRLO42xcKhBnn/n27FeWQYwQCOxPOa+jwY80KlKVr6Piudl4sk44oay+zb7vR7OeERNDGFAxgKMAewrqtniLJ0U7cnhFtPdOm/wvVYriG2ErTn8nMYCzsME5Xmpy44Zo6yOnBzcmB3Ai9L8C/D/QLS2t9BtbmyjsEZLcQXcp8sNwcAnB98GsHwuNVKB0r8tyb9kT4Yfh9stq6Xqmh7jvX1VLnUZ7iCQEowt5DkIw4HV3zU4OHiww1krHy/y2bLNSg3H/AAaxsnZO3tiaaNJ23p4s7IymcQLIxVWOM9OScA4HHatYQjBVjVI8zPnnyJ75PfosZ+HJUFas5mCYs/wrwPegDsSSIcli1EuwFyB/WkgGs6PMjIp6fnVoCIGnXIctlT/atNhpBtL81y5Mg4PTihsGSCSI/wALDsenmp9hVgLaWWYMksYTp9vWhjob3D+Wx6nxk/sKaAQ8qHpKy80KwoW6hQCJBz7U7GLgJdTwfvSAUvmD4ASKAPecAcB+r3xQATAkA+Ij60MBRgi/UsnUfalY0Y94jbG3frnihp2tW01tb7dsrDqfqkYvJdFscIBjhfWuLLxPLmU7pH0f4/8ALvhYNI+/RG+Ivh3u7X9rQ3G07wS6vpkv5qzFw3QJGHBTI4wRxz71OXgQa/X2a4/zcpyqZrG0bWaLS7Jbqz/LTLbRedEDkJIVBYZ9ec12JOEEmeByJ7TdeicngMkfloSOe9FnOIFkRGiSZbpH70WOxUVisbGQJ057U7ROzC2tmFnMzMSSMHNS5CbsfdKA8nt7VNv4EcdlxgYqkFAQwU5/sKAo8ZXIyFxigKPFpSmekmjokAzA5PTVIdAnk+EqRkGrooFbQIGBj4AOSDSYmrC3CqjqzdS5NCBdEfDLc4ADd/U1dIYi5gkLF3OSfSi0AmOB5AVxn50MA8dk68sAPkTUgPFDiLAdRz7YNJugOflfOIdjjFLYAoskTlT2ocgDJEpGGAqbYCGgRTjFFsaBTWkEyFZFDDPY0rZSbXoXFahSGQYx6U7bBt0FUIDlVAPakRbFkKRgtigDuEAGGoA95hbKn070qASTjAU06+wOuTwATQAjqccGgBYK49vnQARZEAHSATRTfoTFBwckDFKvskbyPGqkkDNUmAzaVMDGD9a0LG5uEDNnqDA+lACZJmOHUnjsPU1SQHUgkLhUft6Um0A9isyy/wAxiajYAhsmUdceFA9BS2sDgjweUIPvnNAHGDFgoKkUWFBAoIPUOB3NL5ADcvCkXWo5PaqXsDtpdoyjzB0vjH1qZRr0FDo9AGR681KGhQUSc9PFMTQJoirDpb6fKgoUkcjZ+L6UCZxwy566F2KgWMnA7HmnTKFxtzluwpAwhIk5HcUEgTIQekjB96KAUoY8Dk0CsV0H+ofegGckcGIgcg00JDbzeMHt6j51VFCjJFGAxGc0AJTDAsMEHvQgGkqBV6gVLZ7/AC9q0QDXLmTyxwBx96YH/9k=">
                    <p>
                      Text after img
                    </p>
                  </div>-->
                    <!--<button id="cmd" onclick="printIt()">generate PDF</button>-->

            </li>
        </ul>
                    
                    
      </div>
     
      <!-- / main body -->
      <div class="clear"></div>
    </main>
  </div>
</div>
 
 

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    
    <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved </p>
     
  </div>
</div>
<!-- JAVASCRIPTS --> 
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="./layout/scripts/jquery.fitvids.min.js"></script> 
<script src="./layout/scripts/jquery.mobilemenu.js"></script>

<script src="./layout/scripts/tabslet/jquery.tabslet.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
</body>
</html>
<% }catch(Exception e) {
    e.printStackTrace();
} %>
